enablePlugins(ScalaJSPlugin)

name := "Scala.js Tutorial"

scalaJSUseRhino in Global := false

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.0"
libraryDependencies += "be.doeraene" %%% "scalajs-jquery" % "0.9.0"

// plain JS dependencies
// create "scala-js-{name}-jsdeps.js"
skip in packageJSDependencies := false
jsDependencies += "org.webjars" % "jquery" % "2.1.4" / "2.1.4/jquery.js"

// create DOM for testing
jsDependencies += RuntimeDOM

// unit testing
libraryDependencies += "com.lihaoyi" %%% "utest" % "0.3.0" % "test"
testFrameworks += new TestFramework("utest.runner.Framework")

// create automatic launcher
persistLauncher in Compile := true
persistLauncher in Test := false
